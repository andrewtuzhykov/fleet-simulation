
import numpy as np
import osmnx as ox
import networkx as nx
from closest_road_hadler import *

import matplotlib.pyplot as plt


center = (52.54515,13.49000)

# you have to add network_type='drive' parameter to retrieve only drivable roads
G = ox.graph_from_point(center, distance=500)

departure = (52.54528,13.48788)
destination = (52.54379,13.48543)



_ , departure_road_segment_edge1, departure_road_segment_edge2 = get_nearest_road(G, departure)
_, destination_road_segment_edge1, destination_road_segment_edge2 = get_nearest_road(G, destination)

departure_node = get_nearest_segment_edge(G, departure, departure_road_segment_edge1, departure_road_segment_edge2) 
destination_node  = get_nearest_segment_edge(G, destination, destination_road_segment_edge1, destination_road_segment_edge2)

route = nx.shortest_path(G, departure_node, destination_node)


fig, ax = ox.plot_graph_route(G, route, fig_height=10, fig_width=10, 
                    show=False, close=False, 
                    edge_color='black',
                    orig_dest_node_color='green',
                    route_color='green')

ax.scatter(departure[1], departure[0], c='red', s=100)
ax.scatter(destination[1], destination[0], c='blue', s=100)

plt.show()


