import osmnx as ox
import networkx as nx
from shapely.geometry import Point
from sklearn.neighbors import KDTree


def get_nearest_segment_edge(G,point,edge1,edge2):
    """
    Return the nearest edge of road segment
    ----------
    G : networkx multidigraph
    edge1, edge2 : int
           node id in the graph
    point : tuple
        The (lat, lng) or (y, x) point for which we will find the nearest node
        in the graph

    Returns
    -------
    nearest_edge : int
        id of the nearest node of road segment
    """
    nodes, _ = ox.graph_to_gdfs(G)
    ids = [edge1, edge2]
    road_nodes = nodes.loc[nodes['osmid'].isin([str(edge1),str(edge2)])]

    tree = KDTree(road_nodes[['y', 'x']], metric='euclidean')
    nearest_edge_idx = tree.query([point], k=1, return_distance=False)[0]
    nearest_edge = road_nodes.iloc[nearest_edge_idx].index.values[0]
    
    return nearest_edge






def get_nearest_road(G, point):
    """
    Return the nearest road to a pair of coordinates.
    Pass in the graph representing the road network and a tuple with the
    coordinates. We first get all the roads in the graph. Secondly we
    compute the distance from the coordinates to the segments determined
    by each road. The last step is to sort the road segments in ascending
    order based on the distance from the coordinates to the road.
    In the end, the first element in the list of roads will be the closest
    road that we will return as a tuple containing the shapely geometry and
    the u, v nodes.
    Parameters
    ----------
    G : networkx multidigraph
    point : tuple
        The (lat, lng) or (y, x) point for which we will find the nearest node
        in the graph
    Returns
    -------
    closest_road_to_point : tuple
        A geometry object representing the segment and the coordinates of the two
        nodes that determine the road section, u and v, the OSM ids of the nodes.
    """
    gdf = ox.graph_to_gdfs(G, nodes=False, fill_edge_geometry=True)
    graph_roads = gdf[["geometry", "u", "v"]].values.tolist()

    roads_with_distances = [
        (
            graph_road,
            Point(tuple(reversed(point))).distance(graph_road[0])
        )
        for graph_road in graph_roads
    ]

    roads_with_distances = sorted(roads_with_distances, key=lambda x: x[1])
    closest_road_to_point = roads_with_distances[0][0]
    return closest_road_to_point