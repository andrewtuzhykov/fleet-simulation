import osmnx as ox

# One of the great things about OSMnx is 
# that it integrates with other great projects such as NetworkX 
# https://networkx.github.io/
import networkx as nx
import numpy as np

berlin_center = (52.54514,13.49003)
G = ox.graph_from_point(berlin_center, distance=750, network_type='all')

# shortest path between two random nodes (not points!)
route = nx.shortest_path(G, np.random.choice(G.nodes), np.random.choice(G.nodes))
ox.plot_graph_route(G, route)
